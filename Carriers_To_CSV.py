#!/usr/bin/python

import sys
import requests
import json
import pandas as pd

headers = {'X-API-KEY': 'TspZ1dqvSq51pQNNF5c0o4SgA2nmOITLCWvezBU5'}
args = sys.argv[1:]

for arg in args:
	response = requests.get('https://qtup5470re.execute-api.us-east-1.amazonaws.com/prod/get_load_matches?carrier_id={}'.format(arg), headers=headers)
	df = pd.read_json(response.content)
	df.set_index('Lane').to_csv('{}.csv'.format(arg))
